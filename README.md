# eZ-CPM ITX
**!!! NOT WORKING YET !!!**
## Introduction  
The eZ-CPM ITX is a highly integrated hardware platform for running CP/M and its derivatives.   
It is based on the Zilog eZ80-F91. It can run Z80 binary code natively, has a 16 MB address range for RAM and a separate 16-bit IO space. Integrated peripherals include independent SPI, I²C and UART modules, a battery powered RTC, a MII network interface. It also contains 256 kB flash and 16 kB SRAM.

## Supported hardware   
Thanks to the high level of integration of the eZ80, only few external peripherals are necessary, but many are supported.   
The eZ-CPM ITX has provisions for 8x2MB SRAM, but it can be used with only 1x2MB, which is still plenty for an 8-bit system. The SPI interface can be connected to one or two µSD card sockets and/or onboard SPI flash for mass storage.  
The second big part on the eZ-CPM ITX is a FDC37C669-MT Super-IO controller. It provides 2 additional UARTs, a parallel port, IDE, and can drive up to 4 Floppy drives.   
The 80-pin RCbus socket opens up a wide variety of expansion cards or custom circuits. 
